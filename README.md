Universal Package Manager (UPM)
===============================
Copyright 2015-2016 by Ethan Ruffing. All rights reserved.

This is a universal package management system designed to simplify the process
of moving between Linux and UNIX distributions. It is not intended to replace
any distribution's native system (it does not even reference a unique online
databse), but to act as a layer of abstraction which will provide a common
experience across all systems.

The primary purpose of this system is to provide a unified wrapper for use in
scripting, enabling developers to write system-independent scripts that use
package managers.

