NOTICE
======
This program is Copyright 2015-2016 by Ethan Ruffing. All rights reserved.

If you wish to use this program in your own script, contact the developer
to request a licensed version of the source code.

Ethan Ruffing
ethan@ethanruffing.com

