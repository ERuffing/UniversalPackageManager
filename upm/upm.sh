#!/bin/sh
# 
# Thit file is part of the Universal Package Manger by Ethan Ruffing.
# Copyright 2015-2016 by Ethan Ruffing. All rights reserved. See the
# accompanying NOTICE.md.
# 
# The main script for the Universal Package Manager

upmBinDir="."
source $upmBinDir/conf_loc.sh
source $upmBinDir/is_root.sh

function upm_run {
	SUCCESS= $( false )
	for f in $( conf_loc )
	do
		OLDIFS=$IFS
		IFS=";"
		while read ROOT COMMAND INSTALL REMOVE UPDATE UPGRADE CLEAN
		do
			[[ $ROOT = \#* ]] && continue # Skip comment lines
			if [ ! $SUCCESS ];
			then
				if [ "$ROOT" = "true" ] && [ is_root ];
				then
					if hash "$COMMAND" 2>/dev/null;
					then
						key="$1"
						shift
						case $key in
							install)
								$COMMAND $INSTALL "$@"
								SUCCESS= $( true )
								;;
							remove)
								$COMMAND $REMOVE "$@"
								SUCCESS= $( true )
								;;
							update)
								$COMMAND $UPDATE "$@"
								SUCCESS= $( true )
								;;
							upgrade)
								$COMMAND $UPGRADE "$@"
								SUCCESS= $( true )
								;;
							clean)
								$COMMAND $CLEAN "$@"
								SUCCESS= $( true )
								;;
						esac
					fi
					break
				fi
			else
				break
			fi
		done < "$f"
		IFS=$OLDIFS
		if [ $SUCCESS ];
		then
			break
		fi
	done

	if [ ! $SUCCESS ];
	then
		echo "Failed to find a useable package manager."
	fi

	return $SUCCESS
}

# Process scirpt arguments and respond accordingly.
while [[ $# > 0 ]]
do
	key="$1"
	shift	
	case $key in
		-h|--help|help)
			"$upmBinDir"/help.sh # Display help
			exit 0
			;;
		install|remove|update|upgrade|clean)
			SUCC=$( upm_run "$key" "$@" ) # Pass to function
			echo $SUCC
			;;
		*)
			echo "Invalid command: $key"
			exit 1
			;;
	esac
	
	shift	# Move to next argument
done

