#!/bin/sh
#
# This file is part of the Universal Package Manger by Ethan Ruffing.
# Copyright 2015-2016 by Ethan Ruffing. All rights reserved. See the
# accompanying NOTICE.md.
#
# The function provided here creates a string of the directory of folders to
# look in for package manager configuration files.

function conf_loc {
	echo './conf/*.pm'
}

