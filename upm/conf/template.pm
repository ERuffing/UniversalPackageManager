# This is a template package manager definition file. The first item on each
# line should be whether the specified command requires elevated priveleges to
# execute. Following that should be the command for the package manager,
# followed by the install, remove, update, cleanup, and upgrade arguments for
# the package manager. Note that the boolean value for ROOT should be written
# in all lower-case (i.e. 'true' or 'false').
#
# Best practice is to have one package manager definition per file, though
# multiple are supported.
#
# ROOT;	COMMAND;	INSTALL;	REMOVE;	UPDATE;	UPGRADE;	CLEANUP;
# =============================================================================

