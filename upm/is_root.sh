#!/bin/sh
#
# This file is part of the Universal Package Manger by Ethan Ruffing.
# Copyright 2015-2016 by Ethan Ruffing. All rights reserved. See the
# accompanying NOTICE.md.
#
# This function will determine if the script is being run with elevated (root)
# priveleges.

function is_root {
	return [ "$EUID" -eq 0 ]
}

